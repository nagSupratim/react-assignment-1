import './Article.css';

function ArticleFunctional() {
  return (
    <article>
      <h2>This is created using functional Component</h2>
      <p>This is done using external CSS</p>
      <p style={{ color: 'blue' }}>This is done using inline CSS</p>
    </article>
  );
}

export default ArticleFunctional;
