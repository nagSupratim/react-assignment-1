import { Component } from 'react';
import './Article.css';

class ArticleClassBased extends Component {
  render() {
    return (
      <article style={{ background: 'pink' }}>
        <h2>This is created using class Component</h2>
        <p>This is done using external CSS</p>
        <p style={{ color: 'blue' }}>This is done using inline CSS</p>
      </article>
    );
  }
}

export default ArticleClassBased;
