import Section from './Section';
import './Container.css';

function Container() {
  return (
    <div className="container">
      <Section type="func" />
      <Section type="clas" />
    </div>
  );
}
export default Container;
