import { useState } from 'react';
import Button from '../UI/Button';
import ArticleClassBased from '../article/ArticleClassBased';
import ArticleFunctional from '../article/ArticleFunctional';
import './Section.css';

function Section(props) {
  const [isVisible, setIsVisible] = useState(false);
  const clickHandler = () => {
    setIsVisible((prev) => !prev);
  };
  const btnText =
    props.type === 'func'
      ? 'To see styling in functional component'
      : 'To see styling in class component';
  return (
    <div className={`section ${props.type}`}>
      <Button clickHandler={clickHandler} buttonText={btnText} />
      {props.type === 'func' && isVisible && <ArticleFunctional />}
      {props.type === 'clas' && isVisible && <ArticleClassBased />}
    </div>
  );
}
export default Section;
