import './Button.css';
function Button(props) {
  return (
    <button className={props.className} onClick={props.clickHandler}>
      {props.buttonText}
    </button>
  );
}
export default Button;
