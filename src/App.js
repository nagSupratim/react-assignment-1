import Container from './components/container/Container';

function App() {
  return (
    <div>
      <h1
        style={{
          textAlign: 'center',
          paddingBottom: '30px',
          textShadow: '2px 2px 10px cyan',
        }}
      >
        Styling using Functional and Class Component
      </h1>
      <Container />
    </div>
  );
}

export default App;
